package br.unisinos.ia.algoritmog.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

public class MainProcess {

	public static final float TAXA_MUTACAO = 0.1f;
	public static final int TAMANHO_TORNEIO = 40;
	public static final int TAMANHO_GERACAO = 5000;
	public static final int TAMANHO_REPRODUCAO = 200;

	private Integer tamanhoGenoma;
	private Integer quantidadeCidades;
	private Integer idCidadeInicial;
	private Integer modoSelecao;
	private Integer modoCruzamento;
	private Integer modoMutacao;
	private List<Tupla> cidades;
	private int quantidadeIteracoes = 1000;
	private Integer distanciaOtimaMinima;

	public static Integer FITNESS = Integer.MAX_VALUE;

	public MainProcess(String instancia, Integer idCidadeInicial, Integer modoSelecao, Integer modoMutacao, Integer modoCruzamento, Integer distanciaOtimaMinima) throws Exception {
		super();
		this.idCidadeInicial = idCidadeInicial;
		this.modoSelecao = modoSelecao;
		this.distanciaOtimaMinima = distanciaOtimaMinima;
		this.modoMutacao = modoMutacao;
		this.modoCruzamento = modoCruzamento;
		this.cidades = carregarInstancia(instancia);
		this.tamanhoGenoma = quantidadeCidades - 1;
	}

	/**
	 * Método de leitura de texto e montagem de cidades (List<tupla>)
	 * 
	 * @author Cristiano Farias
	 * @since 16 de abr. de 2022
	 * @param instancia
	 * @throws Exception
	 */
	private List<Tupla> carregarInstancia(String instancia) throws Exception {
		List<Tupla> cidades = new ArrayList<>();
		String splited[] = instancia.split("[\n]");
		for (String linha : splited) {
			String[] atts = linha.split(" ");
			if (atts.length != 3) {
				throw new Exception("Linha com menos atributos que o necessário.");
			}
			Integer id = Integer.parseInt(atts[0]);
			Integer x = Integer.parseInt(atts[1]);
			Integer y = Integer.parseInt(atts[2]);
			Tupla tupla = new Tupla(id, x, y);
			cidades.add(tupla);
		}
		this.quantidadeCidades = cidades.size();
		return cidades;
	}

	public Algoritmo iniciar() throws Exception {
		List<Algoritmo> populacao = populacaoInicial();
		Algoritmo melhorGenomaGlobal = populacao.get(0);
		for (int i = 0; i < quantidadeIteracoes; i++) {
			List<Algoritmo> selecionado = selecoes(populacao);
			populacao = criarGeracao(selecionado);
			melhorGenomaGlobal = populacao.stream().min(Comparator.comparing(Algoritmo::getFitness)).orElseThrow(NoSuchElementException::new);
			if (melhorGenomaGlobal.getFitness() < distanciaOtimaMinima) {
				break;
			}
		}
		return melhorGenomaGlobal;
	}

	/**
	 * Inicio.
	 * 
	 * @author Cristiano Farias
	 * @since 16 de abr. de 2022
	 * @param instancia
	 * @param idCidadeInicial
	 * @return
	 * @throws Exception
	 */
	private List<Algoritmo> populacaoInicial() throws Exception {
		List<Algoritmo> populacao = new ArrayList<>();
		for (int i = 0; i < TAMANHO_GERACAO; i++) {
			populacao.add(new Algoritmo(this.cidades, idCidadeInicial));
		}
		return populacao;
	}

	public List<Algoritmo> selecoes(List<Algoritmo> populacao) {
		List<Algoritmo> selecionado = new ArrayList<>();
		for (int i = 0; i < TAMANHO_REPRODUCAO; i++) {
			switch (modoSelecao) {
			case 1: {
				selecionado.add(selecaoUmRoulette(populacao));
				break;
			}
			case 2: {
				selecionado.add(selecaoDoisTournament(populacao));
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + modoSelecao);
			}
		}
		return selecionado;
	}

	/**
	 * Método de seleção 2
	 * 
	 * @author Cristiano Farias
	 * @author Lucas Sommer
	 * @since 16 de abr. de 2022
	 * @param population
	 * @return
	 */
	public Algoritmo selecaoUmRoulette(List<Algoritmo> population) {
		// pega a distancia de todas as soluções já geradas
		int totalFitness = population.stream().map(Algoritmo::getFitness).mapToInt(Integer::intValue).sum();
		Random random = new Random();
		int selectedValue = random.nextInt(totalFitness);

		float recValue = (float) 1 / selectedValue;
		// adiciona as distancias de todas as soluções até ultrapassar o recValue
		// escolhe o genoma que ultrapassou o recValue
		float currentSum = 0;
		for (Algoritmo genome : population) {
			currentSum += (float) 1 / genome.getFitness();
			if (currentSum >= recValue) {
				return genome;
			}
		}
		// Caso isso não aconteça no loop anterior apenas escolher aleatoriamente então
		int selectRandom = random.nextInt(TAMANHO_GERACAO);
		return population.get(selectRandom);
	}

	/**
	 * Método de seleção 1
	 * 
	 * @author Cristiano Farias
	 * @author Lucas Sommer
	 * @since 16 de abr. de 2022
	 * @param populacao
	 * @return
	 */
	public Algoritmo selecaoDoisTournament(List<Algoritmo> populacao) {
		List<Algoritmo> selecionado = pegarElementosAleatorios(populacao, TAMANHO_TORNEIO);
		return selecionado.stream().min(Comparator.comparing(Algoritmo::getFitness)).orElseThrow(NoSuchElementException::new);
	}

	public List<Algoritmo> criarGeracao(List<Algoritmo> populacao) throws Exception {
		List<Algoritmo> geracao = new ArrayList<>();
		int tamanhoGeracaoAtual = 0;
		while (tamanhoGeracaoAtual < TAMANHO_GERACAO) {
			List<Algoritmo> pais = pegarElementosAleatorios(populacao, 2);
			List<Algoritmo> filhos;
			switch (modoCruzamento) {
			case 1: {
				filhos = cruzamentoUm(pais);
				break;
			}
			case 2: {
				filhos = cruzamentoDois(pais);
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + modoCruzamento);
			}
			switch (this.modoMutacao) {
			case 1: {
				filhos.set(0, mutacaoUm(filhos.get(0)));
				filhos.set(1, mutacaoUm(filhos.get(1)));
				break;
			}
			case 2: {
				filhos.set(0, mutacaoDoisScramble(filhos.get(0)));
				filhos.set(1, mutacaoDoisScramble(filhos.get(1)));
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + this.modoMutacao);
			}
			geracao.addAll(filhos);
			tamanhoGeracaoAtual += 2;
		}
		return geracao;

	}

	public List<Algoritmo> cruzamentoUm(List<Algoritmo> pais) throws Exception {
		Random random = new Random();
		int breakpoint = random.nextInt(this.tamanhoGenoma);
		List<Algoritmo> filhos = new ArrayList<>();

		List<Integer> pai1Genome = new ArrayList<>(pais.get(0).getGenoma());
		List<Integer> pai2Genome = new ArrayList<>(pais.get(1).getGenoma());
		// Criando filho 1
		for (int i = 0; i < breakpoint; i++) {
			int newVal;
			newVal = pai2Genome.get(i);
			Collections.swap(pai1Genome, pai1Genome.indexOf(newVal), i);
		}
		filhos.add(new Algoritmo(this.cidades, this.idCidadeInicial, pai1Genome));
		pai1Genome = pais.get(0).getGenoma();
		// Criando Filho 2
		for (int i = breakpoint; i < this.tamanhoGenoma; i++) {
			int newVal = pai1Genome.get(i);
			Collections.swap(pai2Genome, pai2Genome.indexOf(newVal), i);
		}
		filhos.add(new Algoritmo(this.cidades, this.idCidadeInicial, pai2Genome));
		return filhos;
	}

	// Recombinação em dois pontos
	public List<Algoritmo> cruzamentoDois(List<Algoritmo> pais) throws Exception {
		Random random = new Random();
		int breakpoint1 = random.nextInt(this.tamanhoGenoma - 1);
		int breakpoint2 = random.nextInt(this.tamanhoGenoma - 1);

		if ((breakpoint1 == breakpoint2) || (breakpoint1 > breakpoint2)) {
			while ((breakpoint1 == breakpoint2) || (breakpoint1 > breakpoint2)) {
				breakpoint2 = random.nextInt(this.tamanhoGenoma);
			}
		}

		List<Algoritmo> filhos = new ArrayList<>();

		List<Integer> pai1Genome = new ArrayList<>(pais.get(0).getGenoma());
		List<Integer> pai2Genome = new ArrayList<>(pais.get(1).getGenoma());

		// Criando filho 1
		for (int i = breakpoint1; i <= breakpoint2; i++) {
			int newVal;
			newVal = pai2Genome.get(i);
			Collections.swap(pai1Genome, pai1Genome.indexOf(newVal), i);
		}
		filhos.add(new Algoritmo(this.cidades, this.idCidadeInicial, pai1Genome));
		pai1Genome = pais.get(0).getGenoma();

		// Criando filho 2
		for (int i = breakpoint1; i <= breakpoint2; i++) {
			int newVal = pai1Genome.get(i);
			Collections.swap(pai2Genome, pai2Genome.indexOf(newVal), i);
		}

		filhos.add(new Algoritmo(this.cidades, this.idCidadeInicial, pai2Genome));
		return filhos;
	}

	public <E> List<E> pegarElementosAleatorios(List<E> lista, int n) {
		Random r = new Random();
		int length = lista.size();
		if (length < n)
			return null;
		for (int i = length - 1; i >= length - n; --i) {
			Collections.swap(lista, i, r.nextInt(i + 1));
		}
		return lista.subList(length - n, length);
	}

	/**
	 * Troca aleatória
	 * 
	 * @author Cristiano Farias
	 * @since 21 de abr. de 2022
	 * @param processo
	 * @return
	 * @throws Exception
	 */
	public Algoritmo mutacaoUm(Algoritmo processo) throws Exception {
		Random random = new Random();
		float mutate = random.nextFloat();
		if (mutate < MainProcess.TAXA_MUTACAO) {
			List<Integer> genome = processo.getGenoma();
			Collections.swap(genome, random.nextInt(this.tamanhoGenoma), random.nextInt(this.tamanhoGenoma));
			return new Algoritmo(this.cidades, this.idCidadeInicial, genome);
		}
		return processo;
	}

	/**
	 * Seleciona numeros aleatoriamente de 0 até o tamanho do array e após isso pega
	 * valores novamente aleatorios com o limite até o valor selecionado no random
	 * anterior e faz swap dentro do genoma especifico
	 * 
	 * @author Lucas Sommer
	 * @since 17 de abr. de 2022
	 * @param processo
	 * @return
	 * @throws Exception
	 */
	public Algoritmo mutacaoDoisScramble(Algoritmo processo) throws Exception {
		Random random = new Random();
		float mutate = random.nextFloat();
		if (mutate < MainProcess.TAXA_MUTACAO) {
			List<Integer> genome = processo.getGenoma();
			for (int i = 0; i < 4; i++) {
				int rand1 = random.nextInt(processo.getGenoma().size());
				int rand2 = random.nextInt(processo.getGenoma().size());

				while (rand1 >= rand2) {
					rand1 = random.nextInt(processo.getGenoma().size());
					rand2 = random.nextInt(processo.getGenoma().size());
				}

				for (int k = 0; k < 8; k++) {
					int k1 = random.nextInt(rand2 + 1 - rand1) + rand1;
					int k2 = random.nextInt(rand2 + 1 - rand1) + rand1;
					Collections.swap(genome, k1, k2);
				}
			}
			return new Algoritmo(this.cidades, idCidadeInicial, genome);
		}

		return processo;
	}

	/**
	 *
	 * Classe que representa uma tupla da instância do problema.
	 * 
	 * @author Cristiano Farias
	 * @since 16 de abr. de 2022
	 */
	public class Tupla {
		private Integer ID;
		private Integer X;
		private Integer Y;

		public Tupla(Integer iD, Integer x, Integer y) {
			super();
			ID = iD;
			X = x;
			Y = y;
		}

		public Integer getID() {
			return ID;
		}

		public void setID(Integer iD) {
			ID = iD;
		}

		public Integer getX() {
			return X;
		}

		public void setX(Integer x) {
			X = x;
		}

		public Integer getY() {
			return Y;
		}

		public void setY(Integer y) {
			Y = y;
		}

		@Override
		public String toString() {
			return "Tupla [ID=" + ID + ", X=" + X + ", Y=" + Y + "]";
		}

	}

	// berlin52 - solução �tima distancia de 7542
	// pr76 - solução �tima distancia de 108159
	// st70 - solução �tima distancia de 675
	public static void main(String[] args) throws Exception {
		System.out.println("====================== Algoritmo Genético ===========================");

		String berlin52 = "1 565 575\n" + "2 25 185\n" + "3 345 750\n" + "4 945 685\n" + "5 845 655\n" + "6 880 660\n" + "7 25 230\n" + "8 525 1000\n" + "9 580 1175\n"
				+ "10 650 1130\n" + "11 1605 620 \n" + "12 1220 580\n" + "13 1465 200\n" + "14 1530 5\n" + "15 845 680\n" + "16 725 370\n" + "17 145 665\n" + "18 415 635\n"
				+ "19 510 875  \n" + "20 560 365\n" + "21 300 465\n" + "22 520 585\n" + "23 480 415\n" + "24 835 625\n" + "25 975 580\n" + "26 1215 245\n" + "27 1320 315\n"
				+ "28 1250 400\n" + "29 660 180\n" + "30 410 250\n" + "31 420 555\n" + "32 575 665\n" + "33 1150 1160\n" + "34 700 580\n" + "35 685 595\n" + "36 685 610\n"
				+ "37 770 610\n" + "38 795 645\n" + "39 720 635\n" + "40 760 650\n" + "41 475 960\n" + "42 95 260\n" + "43 875 920\n" + "44 700 500\n" + "45 555 815\n"
				+ "46 830 485\n" + "47 1170 65\n" + "48 830 610\n" + "49 605 625\n" + "50 595 360\n" + "51 1340 725\n" + "52 1740 245";
		String pr76 = "1 3600 2300\n" + "2 3100 3300\n" + "3 4700 5750\n" + "4 5400 5750\n" + "5 5608 7103\n" + "6 4493 7102\n" + "7 3600 6950\n" + "8 3100 7250\n"
				+ "9 4700 8450\n" + "10 5400 8450\n" + "11 5610 10053\n" + "12 4492 10052\n" + "13 3600 10800\n" + "14 3100 10950\n" + "15 4700 11650\n" + "16 5400 11650\n"
				+ "17 6650 10800\n" + "18 7300 10950\n" + "19 7300 7250\n" + "20 6650 6950\n" + "21 7300 3300\n" + "22 6650 2300\n" + "23 5400 1600\n" + "24 8350 2300\n"
				+ "25 7850 3300\n" + "26 9450 5750\n" + "27 10150 5750\n" + "28 10358 7103\n" + "29 9243 7102\n" + "30 8350 6950\n" + "31 7850 7250\n" + "32 9450 8450\n"
				+ "33 10150 8450\n" + "34 10360 10053\n" + "35 9242 10052\n" + "36 8350 10800\n" + "37 7850 10950\n" + "38 9450 11650\n" + "39 10150 11650\n" + "40 11400 10800\n"
				+ "41 12050 10950\n" + "42 12050 7250\n" + "43 11400 6950\n" + "44 12050 3300\n" + "45 11400 2300\n" + "46 10150 1600\n" + "47 13100 2300\n" + "48 12600 3300\n"
				+ "49 14200 5750\n" + "50 14900 5750\n" + "51 15108 7103\n" + "52 13993 7102\n" + "53 13100 6950\n" + "54 12600 7250\n" + "55 14200 8450\n" + "56 14900 8450\n"
				+ "57 15110 10053\n" + "58 13992 10052\n" + "59 13100 10800\n" + "60 12600 10950\n" + "61 14200 11650\n" + "62 14900 11650\n" + "63 16150 10800\n"
				+ "64 16800 10950\n" + "65 16800 7250\n" + "66 16150 6950\n" + "67 16800 3300\n" + "68 16150 2300\n" + "69 14900 1600\n" + "70 19800 800\n" + "71 19800 10000\n"
				+ "72 19800 11900\n" + "73 19800 12200\n" + "74 200 12200\n" + "75 200 1100\n" + "76 200 800";
		String st70 = "1 64 96\n" + "2 80 39\n" + "3 69 23\n" + "4 72 42\n" + "5 48 67\n" + "6 58 43\n" + "7 81 34\n" + "8 79 17\n" + "9 30 23\n" + "10 42 67\n" + "11 7 76\n"
				+ "12 29 51\n" + "13 78 92\n" + "14 64 8\n" + "15 95 57\n" + "16 57 91\n" + "17 40 35\n" + "18 68 40\n" + "19 92 34\n" + "20 62 1\n" + "21 28 43\n" + "22 76 73\n"
				+ "23 67 88\n" + "24 93 54\n" + "25 6 8\n" + "26 87 18\n" + "27 30 9\n" + "28 77 13\n" + "29 78 94\n" + "30 55 3\n" + "31 82 88\n" + "32 73 28\n" + "33 20 55\n"
				+ "34 27 43\n" + "35 95 86\n" + "36 67 99\n" + "37 48 83\n" + "38 75 81\n" + "39 8 19\n" + "40 20 18\n" + "41 54 38\n" + "42 63 36\n" + "43 44 33\n" + "44 52 18\n"
				+ "45 12 13\n" + "46 25 5\n" + "47 58 85\n" + "48 5 67\n" + "49 90 9\n" + "50 41 76\n" + "51 25 76\n" + "52 37 64\n" + "53 56 63\n" + "54 10 55\n" + "55 98 7\n"
				+ "56 16 74\n" + "57 89 60\n" + "58 48 82\n" + "59 81 76\n" + "60 29 60\n" + "61 17 22\n" + "62 5 45\n" + "63 79 70\n" + "64 9 100\n" + "65 17 82\n" + "66 74 67\n"
				+ "67 10 68\n" + "68 48 19\n" + "69 83 86\n" + "70 84 94";

		// Algoritmo mainProcess = new Algoritmo(berlin52, 11);

		MainProcess mainProcess = new MainProcess(berlin52, 7, 2, 1, 1, 7542);
		// MainProcess mainProcess = new MainProcess(pr76, 11, 2, 1, 1, 108159);
		// MainProcess mainProcess = new MainProcess(st70, 11, 2, 1, 1, 675);

		mainProcess.cidades.forEach(c -> System.out.println(c));

		System.out.println("======> Numero de Cidades: " + mainProcess.quantidadeCidades);

		System.out.println(mainProcess.iniciar());
	}
}
