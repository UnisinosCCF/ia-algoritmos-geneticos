package br.unisinos.ia.algoritmog.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import br.unisinos.ia.algoritmog.controller.MainProcess.Tupla;

/**
 * 
 * @author Cristiano Farias
 * @since 16 de abr. de 2022
 */
public class Algoritmo {

	private List<Tupla> cidades;
	private int quantidadeDeCidades;
	private int cidadeInicial;
	private List<Integer> genoma;
	private int fitness;

	public Algoritmo(List<Tupla> cidades, Integer idCidadeInicial) throws Exception {
		super();
		this.cidades = cidades;
		this.quantidadeDeCidades = this.cidades.size();
		this.cidadeInicial = idCidadeInicial;
		this.genoma = gerarGenoma();
		this.calcularFitness();
	}

	public Algoritmo(List<Tupla> cidades, int idCidadeInicial, List<Integer> genoma) throws Exception {
		super();
		this.cidades = cidades;
		this.cidadeInicial = idCidadeInicial;
		this.genoma = genoma;
		this.quantidadeDeCidades = this.cidades.size();
		this.calcularFitness();
	}

	public List<Integer> getGenoma() {
		return genoma;
	}

	/**
	 * calcula a distância entre as cidades(tuplas) para um determinado genoma
	 * 
	 * @author Lucas Sommer
	 * @since 16 de abr. de 2022
	 * @return
	 */
	private void calcularFitness() {
		// System.out.print("======> Genoma: ");
		// this.genoma.forEach(g -> System.out.print(g + " "));
		int fitness = 0;
		int cidadeAtual = cidadeInicial;
		for (int gene : genoma) {
			fitness += distanciaEuclidiana(buscarTupla(cidadeAtual), buscarTupla(gene));
			cidadeAtual = gene;
		}
		fitness += distanciaEuclidiana(buscarTupla(cidadeAtual), buscarTupla(cidadeInicial));
		this.fitness = fitness;

		// Print de fitness menor que o anterior
		if (getFitness() < MainProcess.FITNESS) {
			System.out.println("\n======> Fitness: " + this.getFitness());
			MainProcess.FITNESS = getFitness();
		}
	}

	/**
	 * Gera um genoma aleat�rio excluindo a cidade inicial da geração
	 * 
	 * @author Lucas Sommer
	 * @since 16 de abr. de 2022
	 * @return
	 */
	private List<Integer> gerarGenoma() {
		List<Integer> resultado = new ArrayList<Integer>();
		for (int i = 0; i < quantidadeDeCidades; i++) {
			if (cidades.get(i).getID() != cidadeInicial)
				resultado.add(cidades.get(i).getID());
		}
		Collections.shuffle(resultado);
		return resultado;
	}

	public int getFitness() {
		return fitness;
	}

	/**
	 * 
	 * @author Lucas Sommer
	 * @since 16 de abr. de 2022
	 * @param a
	 * @param b
	 * @return
	 */
	private double distanciaEuclidiana(Tupla a, Tupla b) {
		double xs = Math.pow((a.getX() - b.getX()), 2);
		double ys = Math.pow((a.getY() - b.getY()), 2);
		return Math.sqrt((xs + ys));
	}

	/**
	 * Retorna a tupla na lista de cidades passando a id por parametro.
	 * 
	 * @author Cristiano Farias
	 * @since 16 de abr. de 2022
	 * @param id
	 * @return
	 */
	private Tupla buscarTupla(Integer id) {
		return this.cidades.stream().filter(c -> c.getID().intValue() == id.intValue()).findAny().orElseThrow(NoSuchElementException::new);
	}

	@Override
	public String toString() {
		return "Algoritmo \n\tquantidadeDeCidades=" + quantidadeDeCidades + ", \n\tcidadeInicial=" + cidadeInicial + ", \n\tgenoma=" + genoma + ", \n\tfitness=" + fitness + "";
	}

}
